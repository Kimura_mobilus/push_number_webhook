defmodule PushNumberWebhook do
  @moduledoc """
  Documentation for PushNumberWebhook.
  """

  @doc """
  Hello world.

  ## Examples

      iex> PushNumberWebhook.hello()
      :world

  """
  def hello do
    :world
  end
end
